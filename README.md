# TvShows

> An Angular project for showing the tv show list based on their ratings and categories

## Framework selection

> Advantages of using Angular
* Easy to Understand
* Simple Integration
* Flexibility
* Two-Way Communication
* Routing
* Great Tooling

## Build Setup

Using @angular/cli @9.1.5

## Pre-requisite
* Node
* TypeScript

### Installation
NPM is the recommended installation method when building large scale applications with Angular. It pairs nicely with module bundlers such as Webpack or Browserify. 

#### Latest stable
``` bash
$ sudo npm i -g @angular/cli
```
## Clone repo
``` bash
# clone repository
$git clone https://github.com/maheshchikkam/tv-shows.git

# Navigate to tv-shows folder
$cd tv-shows

# install dependencies
npm install
```

## Development

### Run the project

``` bash
npm start
```

Project is now running on localhost:4200

# Build for production with minification

``` bash
npm run build:prod
```

# run unit tests

``` bash
npm run test
```

# Run e2e tests

``` bash
npm run e2e
```

### Useful command to generate new items

``` bash
Creating new component : ng g component --componentName--
Creating new class : ng g class --className--
Creating new library : ng g library --libraryName--
Creating new module : ng g module --moduleName--
Creating new service : ng g service --serviceName--
Creating new interface : ng g interface --interfaceName--
```

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

### Running the tests

* To run the tests run

``` bash
npm test
```

Running the unit tests generates coverage info in the `/coverage` directory.

### Writing tests

Our tests adhere to the [recommended testing strategy](https://angular.io/guide/testing#isolated-unit-tests-vs-the-angular-testing-utilities) by the Angular team. This strategy means we test components differently from services and pipes.

_How do you test a component?_

There are two ways to test components.

1. (Unit Test) You just want to test the TypeScript functionality.
2. (Integration Test) You want to test the components' interaction with Angular (templates etc).

If you just want to test the functionality you can test the component identically to a service or pipe.
If you also want to test the interaction with Angular you need to use the Angular Testing Utilities. This allows us to test the interaction of the component with Angular's templates.

_How do you test a service/pipe?_

Test it like you would normally test JavaScript/TypeScript. By creating instances of the service with `new Service()`.

_How do you name your test cases?_

The outermost `describe` should be called the same as the class you're testing. Each method should have it's own `describe` block and should be the same as the name of the method, excluding the brackets. Lastly each test case should be humanly readable, no pseudocode and should adhere to the `should ... when` format as much as possible.

_How do you structure your test case?_

Use the `AAA` pattern:

1. **Arrange** - Setup the state of the application
2. **Act** - Invoke everything to change the state to what you want to test
3. **Assert** - Check that the application has responded as expected

_How do you structure your unit test?_

describe (name of code under test)
    | beforeEach (global test setup)
    |
    | afterEach (global test teardown)
    |
    | describe (name of method)
        |
        | it (name of test case)
            | Arrange
            | Act
            | Assert
        | ...
    | ...
