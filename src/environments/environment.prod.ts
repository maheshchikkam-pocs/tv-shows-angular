export const environment = {
  production: true,
  urls: {
    tvmazeApi: 'https://api.tvmaze.com'
  }
};
