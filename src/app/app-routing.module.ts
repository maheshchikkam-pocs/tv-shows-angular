import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TvShowAllListComponent } from './components/tv-show-all-list/tv-show-all-list.component';
import { TvShowCategoryListComponent } from './components/tv-show-category-list/tv-show-category-list.component';
import { TvShowDetailsComponent } from './components/tv-show-details/tv-show-details.component';

const routes: Routes = [
  {
    path: 'all-shows',
    component: TvShowAllListComponent
  },
  {
    path: 'categorised-shows',
    component: TvShowCategoryListComponent
  },
  {
    path: 'details/:id',
    component: TvShowDetailsComponent
  },
  { path: '', redirectTo: 'all-shows', pathMatch: 'full' },
  { path: '**', redirectTo: 'all-shows', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
