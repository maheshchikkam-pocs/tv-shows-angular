import { Component, OnInit } from '@angular/core';
import { TvShowsService } from '../../services/tv-shows.service';
import { TvShow } from '../../models/tv-show';
import { finalize } from 'rxjs/operators';
import { orderBy } from 'lodash';

@Component({
  selector: 'app-tv-show-all-list',
  templateUrl: './tv-show-all-list.component.html',
  styleUrls: ['./tv-show-all-list.component.scss']
})
export class TvShowAllListComponent implements OnInit {

  public tvShows: TvShow[];
  public loading = false;

  constructor(private tvShowsService: TvShowsService) { }

  ngOnInit(): void {
    this.getAllTvShows();
  }

  public getAllTvShows(): void {
    this.loading = true;
    this.tvShowsService.tvShowsList()
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe((tvShows: TvShow[]) => {
        this.tvShows = orderBy(tvShows, 'rating.average', 'desc');
      });
  }

}
