import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';

import { TvShowAllListComponent } from './tv-show-all-list.component';
import { TvShowsService } from '../../services/tv-shows.service';
import { TvShowsServiceMock } from '../../services/tv-shows.service.mock';
import { TvShow } from 'src/app/models/tv-show';

describe('TvShowAllListComponent', () => {
  let component: TvShowAllListComponent;
  let fixture: ComponentFixture<TvShowAllListComponent>;
  let publicComponent: any;

  const tvShowsServiceMock = new TvShowsServiceMock();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [TvShowAllListComponent],
      providers: [{ provide: TvShowsService, useValue: tvShowsServiceMock }],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TvShowAllListComponent);
    publicComponent = component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('searching for tv shows', () => {
    spyOn(publicComponent.tvShowsService, 'tvShowsList').and.callThrough();
    component.getAllTvShows();
    expect(component.tvShows.length).toBeGreaterThan(0);
  });
});
