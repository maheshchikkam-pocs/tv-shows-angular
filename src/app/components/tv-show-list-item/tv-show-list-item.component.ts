import { Component, Input } from '@angular/core';
import { TvShow } from '../../models/tv-show';
import { Router } from '@angular/router';
@Component({
  selector: 'app-tv-show-list-item',
  templateUrl: './tv-show-list-item.component.html',
  styleUrls: ['./tv-show-list-item.component.scss']
})
export class TvShowListItemComponent {

  @Input()
  public tvShow: TvShow;

  constructor(private router: Router) { }

  public navigateToShowDetail(id: number): void {
    this.router.navigate(['details', id]);
  }

}
