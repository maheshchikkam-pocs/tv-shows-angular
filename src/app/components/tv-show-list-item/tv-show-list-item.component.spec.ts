import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TvShowListItemComponent } from './tv-show-list-item.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';

describe('TvShowListItemComponent', () => {
  let component: TvShowListItemComponent;
  let fixture: ComponentFixture<TvShowListItemComponent>;
  let publicComponent: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [TvShowListItemComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TvShowListItemComponent);
    publicComponent = component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('user should naviagte to details pages when selected the option', () => {
    spyOn(publicComponent.router, 'navigate').and.returnValue(true);
    component.navigateToShowDetail(1);
    expect(publicComponent.router.navigate).toHaveBeenCalledWith(['details', 1]);
  });
});
