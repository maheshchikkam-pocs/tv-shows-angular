import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { MatLegacyAutocompleteModule as MatAutocompleteModule } from '@angular/material/legacy-autocomplete';

import { SearchComponent } from './search.component';
import { TvShowsService } from '../../services/tv-shows.service';
import { TvShowsServiceMock } from '../../services/tv-shows.service.mock';

describe('SearchComponent', () => {
  let component: SearchComponent;
  let publicComponent: any;
  let fixture: ComponentFixture<SearchComponent>;

  const tvShowsServiceMock = new TvShowsServiceMock();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule, MatAutocompleteModule],
      declarations: [SearchComponent],
      providers: [{ provide: TvShowsService, useValue: tvShowsServiceMock }],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    publicComponent = component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('searching for tv shows', () => {
    spyOn(publicComponent.tvShowsService, 'searchResults').and.callThrough();
    component.searched();
    expect(component.searchOptions).toEqual([{}]);
  });

  it('should return the selected value', () => {
    const name = 'Mahesh';
    expect(component.displayFn(name)).toEqual(name);
  });

  it('user should naviagte to details pages when selected the search item', () => {
    spyOn(publicComponent.router, 'navigate').and.returnValue(true);
    component.showSelected({ id: 1 });
    expect(publicComponent.router.navigate).toHaveBeenCalledWith(['details', 1]);
  });


});
