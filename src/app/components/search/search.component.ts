import { Component } from '@angular/core';
import { TvShowsService } from '../../services/tv-shows.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})

export class SearchComponent {

  public searchText = '';
  public searchOptions: any[];

  constructor(private tvShowsService: TvShowsService, private router: Router) {
  }

  public displayFn(name: string): string {
    return name;
  }

  public searched(): void {
    this.tvShowsService.searchResults(this.searchText).subscribe(
      (data) => {
        this.searchOptions = data;
      }
    );
  }

  public showSelected(show): void {
    this.searchText = '';
    this.searchOptions = [];
    this.router.navigate(['details', show.id]);
  }
}
