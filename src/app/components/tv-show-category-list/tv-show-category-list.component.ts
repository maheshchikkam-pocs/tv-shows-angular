import { Component, OnInit } from '@angular/core';
import { TvShowsService } from '../../services/tv-shows.service';
import { TvShow } from '../../models/tv-show';
import { finalize } from 'rxjs/operators';
import { orderBy, groupBy } from 'lodash';

@Component({
  selector: 'app-tv-show-category-list',
  templateUrl: './tv-show-category-list.component.html',
  styleUrls: ['./tv-show-category-list.component.scss']
})
export class TvShowCategoryListComponent implements OnInit {

  public tvShowsByCategory: any;
  public categoryKeys: string[];
  public loading = false;

  constructor(private tvShowsService: TvShowsService) { }

  ngOnInit(): void {
    this.getAllTvShows();
  }

  public getAllTvShows(): void {
    this.loading = true;
    this.tvShowsService.tvShowsList()
      .pipe(
        finalize(() => this.loading = false)
      )
      .subscribe((tvShows: TvShow[]) => {
        this.tvShowsByCategory = groupBy(orderBy(tvShows, 'rating.average', 'desc'), 'genres');
        this.categoryKeys = Object.keys(this.tvShowsByCategory);
      });
  }


}
