import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';

import { TvShowCategoryListComponent } from './tv-show-category-list.component';
import { TvShowsService } from '../../services/tv-shows.service';
import { TvShowsServiceMock } from '../../services/tv-shows.service.mock';
import { TvShow } from 'src/app/models/tv-show';

describe('TvShowCategoryListComponent', () => {
  let component: TvShowCategoryListComponent;
  let fixture: ComponentFixture<TvShowCategoryListComponent>;
  let publicComponent: any;

  const tvShowsServiceMock = new TvShowsServiceMock();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [TvShowCategoryListComponent],
      providers: [{ provide: TvShowsService, useValue: tvShowsServiceMock }],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TvShowCategoryListComponent);
    publicComponent = component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('searching for tv shows', () => {
    spyOn(publicComponent.tvShowsService, 'tvShowsList').and.callThrough();
    component.getAllTvShows();
    expect(component.tvShowsByCategory).toBeDefined();
    expect(component.categoryKeys.length).toBeGreaterThan(0);
  });

});
