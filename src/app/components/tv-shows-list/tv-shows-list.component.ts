import { Component, Input } from '@angular/core';
import { TvShow } from '../../models/tv-show';

@Component({
  selector: 'app-tv-shows-list',
  templateUrl: './tv-shows-list.component.html',
  styleUrls: ['./tv-shows-list.component.scss']
})
export class TvShowsListComponent {
  @Input()
  public tvShows: TvShow[];

  @Input()
  public allShows = false;
}
