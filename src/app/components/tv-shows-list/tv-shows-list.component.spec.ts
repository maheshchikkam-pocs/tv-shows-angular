import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TvShowsListComponent } from './tv-shows-list.component';
import { TvShow } from 'src/app/models/tv-show';

describe('TvShowsListComponent', () => {
  let component: TvShowsListComponent;
  let fixture: ComponentFixture<TvShowsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TvShowsListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TvShowsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set tvShows', () => {
    component.tvShows = [new TvShow()];
    expect(component.tvShows).toBeTruthy();
  });

  it('should set allShows', () => {
    component.allShows = true;
    expect(component.allShows).toBeTruthy();
  });
});
