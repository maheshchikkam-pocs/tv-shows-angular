import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { Location } from '@angular/common';
import { SpyLocation } from '@angular/common/testing';
import { TvShowDetailsComponent } from './tv-show-details.component';
import { TvShowsService } from '../../services/tv-shows.service';
import { TvShowsServiceMock } from '../../services/tv-shows.service.mock';

describe('TvShowDetailsComponent', () => {
  let component: TvShowDetailsComponent;
  let publicComponent: any;
  let fixture: ComponentFixture<TvShowDetailsComponent>;

  const tvShowsServiceMock = new TvShowsServiceMock();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule],
      declarations: [TvShowDetailsComponent],
      providers: [
        { provide: TvShowsService, useValue: tvShowsServiceMock },
        { provide: Location, useClass: SpyLocation }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TvShowDetailsComponent);
    publicComponent = component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have goBack method and should call location.back', () => {
    spyOn(publicComponent.location, 'back');
    component.naviagteToPreviousPage();
    expect(component.naviagteToPreviousPage).toBeDefined();
    expect(publicComponent.location.back).toHaveBeenCalled();
  });

});
