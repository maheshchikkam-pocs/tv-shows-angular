import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { TvShowsService } from '../../services/tv-shows.service';
import { forkJoin } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-tv-show-details',
  templateUrl: './tv-show-details.component.html',
  styleUrls: ['./tv-show-details.component.scss']
})
export class TvShowDetailsComponent implements OnInit {

  public selectedId: number;
  public showDetails: any;
  public seasonDetails: any[];
  public loading = false;

  constructor(private route: ActivatedRoute,
              private tvShowsService: TvShowsService,
              private location: Location) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.getShowDetails(params.id);
    });
  }

  private getShowDetails(showId: number): void {
    this.loading = true;
    forkJoin([
      this.tvShowsService.showDetails(showId),
      this.tvShowsService.seasonDetailsOfShow(showId)
    ]).pipe(finalize(() => (this.loading = false)))
      .subscribe(([showDetails, seasonDetails]) => {
        this.showDetails = showDetails;
        this.seasonDetails = seasonDetails;
      });
  }

  public naviagteToPreviousPage(): void {
    this.location.back();
  }

}
