import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './app.material.module';
import { HeaderComponent } from './components/header/header.component';
import { SearchComponent } from './components/search/search.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { TvShowAllListComponent } from './components/tv-show-all-list/tv-show-all-list.component';
import { TvShowCategoryListComponent } from './components/tv-show-category-list/tv-show-category-list.component';
import { TvShowDetailsComponent } from './components/tv-show-details/tv-show-details.component';
import { TvShowListItemComponent } from './components/tv-show-list-item/tv-show-list-item.component';
import { TvShowsListComponent } from './components/tv-shows-list/tv-shows-list.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TvShowsListComponent,
    TvShowDetailsComponent,
    SearchComponent,
    TvShowListItemComponent,
    TvShowCategoryListComponent,
    TvShowAllListComponent,
    SpinnerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
