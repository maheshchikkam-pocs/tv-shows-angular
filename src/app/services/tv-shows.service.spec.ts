import { TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';

import { TvShowsService } from './tv-shows.service';
import { HttpApiService } from './http-api.service';
import { HttpApiServiceMock } from './http-api.service.mock';

describe('TvShowsService', () => {
  let service: TvShowsService;

  const httpApiServiceMock = new HttpApiServiceMock();

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [
        { provide: HttpApiService, useValue: httpApiServiceMock }
      ],
    });
    service = TestBed.inject(TvShowsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get tvShowsList', () => {
    spyOn(httpApiServiceMock, 'get').and.callThrough();
    service.tvShowsList();
    expect(httpApiServiceMock.get).toHaveBeenCalled();
  });

  it('should get showDetails', () => {
    spyOn(httpApiServiceMock, 'get').and.callThrough();
    service.showDetails(1);
    expect(httpApiServiceMock.get).toHaveBeenCalled();
  });

  it('should get searchResults', () => {
    spyOn(httpApiServiceMock, 'get').and.callThrough();
    service.searchResults('ABC');
    expect(httpApiServiceMock.get).toHaveBeenCalled();
  });

  it('should get seasonDetailsOfShow', () => {
    spyOn(httpApiServiceMock, 'get').and.callThrough();
    service.seasonDetailsOfShow(1);
    expect(httpApiServiceMock.get).toHaveBeenCalled();
  });
});
