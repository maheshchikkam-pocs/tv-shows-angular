import { of, Observable } from 'rxjs';
import { TvShow } from '../models/tv-show';
export class TvShowsServiceMock {

  public tvShowsList(): Observable<TvShow[]> {
    return of([new TvShow()]);
  }

  public showDetails(id: number): Observable<any> {
    return of([{}]);
  }

  public searchResults(searchString: string): Observable<any> {
    return of([{}]);
  }

  public seasonDetailsOfShow(id: number): Observable<any[]> {
    return of([{}]);
  }
}
