import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpApiService } from './http-api.service';

@Injectable({
  providedIn: 'root'
})
export class TvShowsService {

  constructor(private httpApiService: HttpApiService) { }

  public tvShowsList(): Observable<any> {
    return this.httpApiService.get<any>('/shows');
  }

  public showDetails(id: number): Observable<any> {
    return this.httpApiService.get<any>(`/shows/${id}?embed=cast`);
  }

  public searchResults(searchString: string): Observable<any> {
    return this.httpApiService.get<any>(`/search/shows?q=${searchString}`);
  }

  public seasonDetailsOfShow(id: number): Observable<any[]> {
    return this.httpApiService.get<any>(`/shows/${id}/seasons`);
  }

}
